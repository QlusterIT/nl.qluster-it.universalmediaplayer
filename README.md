Use an UPnP Player to find the URL of files on a UPnP Server through browsing (with thumbnails).
When executed it will retrieve the file from de UPnP Server based on the same browsing structure.
This way you will be able to use a UPnP Server to play sounds on speaker systems like Sonos but also on TV's like Samsung or LG.

The way you can browse depends on how the UPnP Server is configured.
Example: 
You can browse to and (it will) save the location to /Music/Artist/Album/B/Bok van Blerk/Afrikanerhart.
When executing the flow, the current URL will be retrieved based on the same /Music/Artists etc.

This way you can use any UPnPServer (Like Synology or an (Asus/Linksys) Router/Switch/Wifi UPnPServer) and use it as a storage place for your Sounds to be used.


Working on playing it on Homey itself.

Thanks for Johan Bendz for the excellent images and icons!